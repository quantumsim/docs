﻿quantumsim.bases.PauliBasis
===========================

.. currentmodule:: quantumsim.bases

.. autoclass:: PauliBasis

   

   
   .. rubric:: Methods

   .. autosummary::
      :toctree:
      
      ~PauliBasis.computational_subbasis
      ~PauliBasis.hilbert_to_pauli_vector
      ~PauliBasis.is_orthonormal
      ~PauliBasis.subbasis
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~PauliBasis.dim_hilbert
      ~PauliBasis.dim_pauli
      ~PauliBasis.superbasis
   
   