
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Lindblad equation &#8212; Quantumsim  documentation</title>
    <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="../_static/alabaster.css" type="text/css" />
    <script id="documentation_options" data-url_root="../" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/doctools.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="High-level interface" href="../reference/index.html" />
    <link rel="prev" title="Pauli Transfer Matrix Manipulation" href="ptm_manipulation.html" />
   
  <link rel="stylesheet" href="../_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <div class="section" id="lindblad-equation">
<h1>Lindblad equation<a class="headerlink" href="#lindblad-equation" title="Permalink to this headline">¶</a></h1>
<p>Constructing quantum operations from realistic Markovian error models is
commonly done using <em>Lindblad equation</em>:</p>
<div class="math notranslate nohighlight">
\[\frac{d\hat{\rho}}{dt} = - i \left[ H, \hat{\rho} \right] +
\sum_j \left[
    L_j \hat{\rho} L_j^\dagger -
    \frac{1}{2}\left\{ L_j^\dagger L_j, \hat{\rho} \right\}
\right],\]</div>
<p>where <span class="math notranslate nohighlight">\(\left[ A, B \right] = AB - BA\)</span> is a commutator,
<span class="math notranslate nohighlight">\(\left\{ A, B \right\} = AB + BA\)</span> is an anticommutator,
and <span class="math notranslate nohighlight">\(\hbar = 1\)</span>.
<em>Lindblad operators</em> <span class="math notranslate nohighlight">\(L_j\)</span> describe the interaction with environment and
therefore can be used to describe error model for the gate <a class="footnote-reference brackets" href="#id3" id="id1">1</a>.
Note the difference in the jump operator definition, in <a class="footnote-reference brackets" href="#id3" id="id2">1</a> they are
multiplied by two compared to the convention we use in Quantumsim.</p>
<p>In order to make use of Lindblad equations, we need to solve the Lindblad
equation and obtain Kraus operators or Pauli transfer matrix in some basis.
Expanding density matrix in some arbitrary orthonormal Pauli basis and using
orthonormality condition, we get:</p>
<div class="math notranslate nohighlight">
\[\dot{\rho}_k = \mathcal{L}_{ki} \rho_i,\]</div>
<p>where</p>
<div class="math notranslate nohighlight">
\[\mathcal{L}_{ki} = \text{tr} \left(
    - i \left[\hat{H}, \hat{P}_i \right] \hat{P}_k
    + \sum_j \left( 2 \hat{L}_j^\vphantom{\dagger}
      \hat{P}_i^\vphantom{\dagger} \hat{L}_j^\dagger
      \hat{P}_k^\vphantom{\dagger}
    - \left\{ \hat{L}_j^\dagger \hat{L}_j^\vphantom{\dagger},
      \hat{P}_i^\vphantom{\dagger} \right\} \hat{P}_k
\right) \right).\]</div>
<p>We call the matrix <span class="math notranslate nohighlight">\(\mathcal{L}\)</span> <em>Pauli Liouville matrix</em>.
Formally we can write the solution of a time evolution as:</p>
<div class="math notranslate nohighlight">
\[\vec{\rho}(t^\prime) =
\exp\left(\mathcal{L}(t^\prime - t)\right) \vec{\rho}(t),\]</div>
<p>where <span class="math notranslate nohighlight">\(\vec{\rho}\)</span> is a Pauli vector, therefore Pauli transfer matrix for
the operation, described by Lindblad equation and acting time T is just:</p>
<div class="math notranslate nohighlight">
\[R(t) = \exp(\mathcal{L} t).\]</div>
<p>Typically the computation of matrix exponent is a quite complex operation, so
amount of its usages during the code execution should be minimized.</p>
<dl class="footnote brackets">
<dt class="label" id="id3"><span class="brackets">1</span><span class="fn-backref">(<a href="#id1">1</a>,<a href="#id2">2</a>)</span></dt>
<dd><p>Sec. 8.4 of Nielson, M. A., and I. L. Chuang.
“Quantum Computation and Quantum Information”
Cambridge University Press (2000).</p>
</dd>
</dl>
</div>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="../index.html">Quantumsim</a></h1>








<h3>Navigation</h3>
<p class="caption"><span class="caption-text">Contents:</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="../install.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="../tutorial/index.html">Tutorial</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="index.html">Mathematical concepts and conventions</a><ul class="current">
<li class="toctree-l2"><a class="reference internal" href="pauli.html">Pauli Bases</a></li>
<li class="toctree-l2"><a class="reference internal" href="ptm_manipulation.html">Pauli Transfer Matrix Manipulation</a></li>
<li class="toctree-l2 current"><a class="current reference internal" href="#">Lindblad equation</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="../reference/index.html">High-level interface</a></li>
<li class="toctree-l1"><a class="reference internal" href="../reference/index.html#low-level-interface">Low-level interface</a></li>
<li class="toctree-l1"><a class="reference internal" href="../reference/index.html#backends">Backends</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="../index.html">Documentation overview</a><ul>
  <li><a href="index.html">Mathematical concepts and conventions</a><ul>
      <li>Previous: <a href="ptm_manipulation.html" title="previous chapter">Pauli Transfer Matrix Manipulation</a></li>
      <li>Next: <a href="../reference/index.html" title="next chapter">High-level interface</a></li>
  </ul></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2018, Brian Tarasinski.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 3.4.3</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="../_sources/architecture/lindblad.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>