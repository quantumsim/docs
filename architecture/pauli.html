
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Pauli Bases &#8212; Quantumsim  documentation</title>
    <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="../_static/alabaster.css" type="text/css" />
    <script id="documentation_options" data-url_root="../" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/doctools.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Pauli Transfer Matrix Manipulation" href="ptm_manipulation.html" />
    <link rel="prev" title="Mathematical concepts and conventions" href="index.html" />
   
  <link rel="stylesheet" href="../_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <div class="section" id="pauli-bases">
<span id="id1"></span><h1>Pauli Bases<a class="headerlink" href="#pauli-bases" title="Permalink to this headline">¶</a></h1>
<div class="section" id="orthonormality-condition">
<h2>Orthonormality condition<a class="headerlink" href="#orthonormality-condition" title="Permalink to this headline">¶</a></h2>
<p>We want to represent any arbitrary :math`d`-dimensional density matrix in a
form:</p>
<div class="math notranslate nohighlight">
\[\hat{\rho} = \sum_{i=0}^N \rho_i \hat{P}_i,\]</div>
<p>where <span class="math notranslate nohighlight">\(\rho_i\)</span> are real numbers.
Any Hermitian <span class="math notranslate nohighlight">\(d \times d\)</span> matrix has <span class="math notranslate nohighlight">\(d^2\)</span> free parameters,
therefore a full basis <span class="math notranslate nohighlight">\(\left\{ \hat{P}_i \right\}\)</span> will have
<span class="math notranslate nohighlight">\(N = d^2\)</span> elements.
We will refer to <span class="math notranslate nohighlight">\(d\)</span> as Hilbert dimensionality (<cite>dim_hilbert</cite> in code),
and <span class="math notranslate nohighlight">\(N\)</span> as Pauli dimensionality (<cite>dim_pauli</cite>).</p>
<p>We will call Pauli basis orthonormal, if it fulfills condition:</p>
<div class="math notranslate nohighlight">
\[\text{tr} \left( \hat{P}_i \hat{P}_j \right) = \delta_{ij}.\]</div>
<p>If this is the case, inverse transformation has the form:</p>
<div class="math notranslate nohighlight">
\[\rho_i = \text{tr} \left( \hat{\rho} \hat{P}_i \right).\]</div>
<p>We operate only in orthonormal bases in Quantumsim, an attempt to create
non-orthonormal basis will raise an exception.</p>
</div>
<div class="section" id="common-bases">
<h2>Common bases<a class="headerlink" href="#common-bases" title="Permalink to this headline">¶</a></h2>
<p>The simplest example of a Pauli basis for <span class="math notranslate nohighlight">\(2 \times 2\)</span> Hermitian matrices
is a basis, that consists of four matrices</p>
<div class="math notranslate nohighlight">
\[\left\{ \hat{I}/\sqrt{2},\ \hat{\sigma}_x/\sqrt{2},\
\hat{\sigma}_y/\sqrt{2},\ \hat{\sigma}_z/\sqrt{2} \right\},\]</div>
<p>unit matrix and three Pauli matrices, normalized by <span class="math notranslate nohighlight">\(\sqrt{d}\)</span>, where
<span class="math notranslate nohighlight">\(d = 2\)</span> is a number of dimensions.
We will refer to this basis as IXYZ basis. It can be generalized to the
arbitrary number of dimensions, if we replace Pauli matrices with generalized
Gell-Mann matrices <a class="footnote-reference brackets" href="#id3" id="id2">1</a>. This basis can be constructed in Quantumsim with
<a class="reference internal" href="../reference/generated/quantumsim.bases.gell_mann.html#quantumsim.bases.gell_mann" title="quantumsim.bases.gell_mann"><code class="xref py py-func docutils literal notranslate"><span class="pre">quantumsim.bases.gell_mann()</span></code></a>.</p>
<p>Another useful choice is formed by the following set of matrices:</p>
<div class="math notranslate nohighlight">
\[\left\{
\left(\hat{I} + \hat{\sigma}_z\right)/2,\ \hat{\sigma}_x/\sqrt{2},\
\hat{\sigma}_y/\sqrt{2},\ \left(\hat{I} - \hat{\sigma}_z\right)/2
\right\}\]</div>
<p>We will refer to this basis as 0XY1 basis. It has an advantage,
that probabilities of measuring 0 and 1 correspond in it to the coefficients
in front of first and last element of this basis, without the necessity to
compute trace explicitly. We can generalize this basis for arbitrary number of
dimensions <span class="math notranslate nohighlight">\(d\)</span>: first we take <span class="math notranslate nohighlight">\(d\)</span> matrices with 1 on a diagonal,
for example for <span class="math notranslate nohighlight">\(d=3\)</span>:</p>
<div class="math notranslate nohighlight">
\[\begin{split}\left\{
\begin{pmatrix}
    1 &amp; 0 &amp; 0 \\
    0 &amp; 0 &amp; 0 \\
    0 &amp; 0 &amp; 0
\end{pmatrix},\
\begin{pmatrix}
    0 &amp; 0 &amp; 0 \\
    0 &amp; 1 &amp; 0 \\
    0 &amp; 0 &amp; 0
\end{pmatrix},\
\begin{pmatrix}
    0 &amp; 0 &amp; 0 \\
    0 &amp; 0 &amp; 0 \\
    0 &amp; 0 &amp; 1
\end{pmatrix},
\ \cdots\right\},\end{split}\]</div>
<p>and then <span class="math notranslate nohighlight">\(d^2-d\)</span> of <span class="math notranslate nohighlight">\(\hat{\sigma}_x\)</span>- and
<span class="math notranslate nohighlight">\(\hat{\sigma}_y\)</span>-like matrices:</p>
<div class="math notranslate nohighlight">
\[\begin{split}\left\{\cdots,\
\begin{pmatrix}
    0 &amp; 1 &amp; 0 \\
    1 &amp; 0 &amp; 0 \\
    0 &amp; 0 &amp; 0
\end{pmatrix},\
\begin{pmatrix}
    0 &amp; -i &amp; 0 \\
    i &amp; 0 &amp; 0 \\
    0 &amp; 0 &amp; 0
\end{pmatrix},\
\begin{pmatrix}
    0 &amp; 0 &amp; 1 \\
    0 &amp; 0 &amp; 0 \\
    1 &amp; 0 &amp; 0
\end{pmatrix},\\
\begin{pmatrix}
    0 &amp; 0 &amp; -i \\
    0 &amp; 0 &amp; 0 \\
    i &amp; 0 &amp; 0
\end{pmatrix},\
\begin{pmatrix}
    0 &amp; 0 &amp; 0 \\
    0 &amp; 0 &amp; 1 \\
    0 &amp; 1 &amp; 0
\end{pmatrix},\
\begin{pmatrix}
    0 &amp; 0 &amp; 0 \\
    0 &amp; 0 &amp; -i \\
    0 &amp; i &amp; 0
\end{pmatrix},
\right\}.\end{split}\]</div>
<p>This basis can be constructed in Quantumsim with
<a class="reference internal" href="../reference/generated/quantumsim.bases.general.html#quantumsim.bases.general" title="quantumsim.bases.general"><code class="xref py py-func docutils literal notranslate"><span class="pre">quantumsim.bases.general()</span></code></a> and is used as a default basis in Quantumsim.</p>
</div>
<div class="section" id="state-representation-in-quantumsim">
<h2>State representation in Quantumsim<a class="headerlink" href="#state-representation-in-quantumsim" title="Permalink to this headline">¶</a></h2>
<p>Suppose we have a system of <span class="math notranslate nohighlight">\(N\)</span> qubits.
Let us fix a separate basis <span class="math notranslate nohighlight">\(\left\{ \hat{P}^{(n)} \right\}\)</span> for each
qubit.
Now, the density matrix can be represented as follows:</p>
<div class="math notranslate nohighlight">
\[\hat{\rho} = \sum_{i_1,\ldots,i_N} \rho_{i_1,\ldots,i_N}
\hat{P}_{i_1}^{(1)} \otimes \ldots \otimes \hat{P}_{i_N}^{(N)},\]</div>
<p>where the sum runs over all elements of this basis.
In the case of full basis <span class="math notranslate nohighlight">\(i_n \in \left[ 1 \ldots d_n^2 \right]\)</span>, but
in general we do not limit ourselves to operating in full bases: if we know from
the circuit, that some basis element is not needed, we will try to throw it away
in the sake of memory and speed.</p>
<dl class="footnote brackets">
<dt class="label" id="id3"><span class="brackets"><a class="fn-backref" href="#id2">1</a></span></dt>
<dd><p><a class="reference external" href="https://en.wikipedia.org/wiki/Generalizations_of_Pauli_matrices">https://en.wikipedia.org/wiki/Generalizations_of_Pauli_matrices</a></p>
</dd>
</dl>
</div>
</div>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="../index.html">Quantumsim</a></h1>








<h3>Navigation</h3>
<p class="caption"><span class="caption-text">Contents:</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="../install.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="../tutorial/index.html">Tutorial</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="index.html">Mathematical concepts and conventions</a><ul class="current">
<li class="toctree-l2 current"><a class="current reference internal" href="#">Pauli Bases</a></li>
<li class="toctree-l2"><a class="reference internal" href="ptm_manipulation.html">Pauli Transfer Matrix Manipulation</a></li>
<li class="toctree-l2"><a class="reference internal" href="lindblad.html">Lindblad equation</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="../reference/index.html">High-level interface</a></li>
<li class="toctree-l1"><a class="reference internal" href="../reference/index.html#low-level-interface">Low-level interface</a></li>
<li class="toctree-l1"><a class="reference internal" href="../reference/index.html#backends">Backends</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="../index.html">Documentation overview</a><ul>
  <li><a href="index.html">Mathematical concepts and conventions</a><ul>
      <li>Previous: <a href="index.html" title="previous chapter">Mathematical concepts and conventions</a></li>
      <li>Next: <a href="ptm_manipulation.html" title="next chapter">Pauli Transfer Matrix Manipulation</a></li>
  </ul></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2018, Brian Tarasinski.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 3.4.3</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="../_sources/architecture/pauli.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>